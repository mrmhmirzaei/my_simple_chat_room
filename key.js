const NodeRSA = require('node-rsa'),key = new NodeRSA();
key.generateKeyPair(2048, 65537);
module.exports = {
    getPublicKey : ()=>{
        return key.exportKey('pkcs8-public-pem');
    },
    getPrivateKey : ()=>{
        return key.exportKey('pkcs8-private-pem');
    }
}