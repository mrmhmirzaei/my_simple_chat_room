const net = require('net').Socket(),key = require('./key'),crypto = require('./crypto'),input = require('readline').createInterface(process.stdin, process.stdout);


const   clientPublicKey = key.getPublicKey(),
        clientPrivateKey = key.getPrivateKey();

let serverPublicKey = null;

net.connect(3000,'127.0.0.1');
net.on('connect',()=>{
    net.write(JSON.stringify({'on':'sendMyInfo','data':{'publicKey':clientPublicKey}}))
})
net.on('data',data=>{
    data = JSON.parse(data.toString());
    if(data['on'] == 'sendServerPublicKey'){
        serverPublicKey = data['data']['serverPublicKey']; 
        getName().then(res=>{
            console.log(`Welcome ${res} !`);
            input.setPrompt(`You> `)
            input.prompt()
            input.on('line',(line)=>{
                if(serverPublicKey !== null){
                    let data = crypto.hash(serverPublicKey,JSON.stringify({fullname : res , message : line}));
                    net.write(JSON.stringify({
                        'on' : 'sendMessage',
                        'data' : {
                            'message' : data
                        }
                    }));
                    input.prompt()
                }
            })
        })       
    }
    else if(data['on'] == "getMessage"){
        let message = data['data']['message'];
            message = crypto.crack(clientPrivateKey,message);
            message = JSON.parse(message);
            console.log(`\r\x1b[K${message.fullname}> ${message.message}`);  
            input.setPrompt(`You> `)
            input.prompt()
    }
    else {}
})
function getName(){
    return new Promise((resolve,reject)=>{
        input.question('What is your name ? ', (line)=>{
            resolve(line);
        })
    })
}