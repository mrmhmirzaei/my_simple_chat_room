const crypto = require('crypto');
module.exports = {
    hash : (publicKey,message)=>{
        return crypto.publicEncrypt({key : publicKey , padding : crypto.RSA_PKCS1_OAEP_PADDING},Buffer.from(message)).toString('base64');
    },
    crack : (privateKey,data)=>{
        return crypto.privateDecrypt({key : privateKey , padding : crypto.RSA_PKCS1_OAEP_PADDING},Buffer.from(data,'base64')).toString();
    }
}