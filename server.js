const net = require('net'),key = require('./key'),crypto = require('./crypto');

const   serverPublicKey = key.getPublicKey(),
        serverPrivateKey = key.getPrivateKey();

let     clients = [];
/*
    clients = {socket : socket , publicKey : clientPublicKey}
*/
const server = net.createServer(socket=>{
    socket.write(JSON.stringify({'on':'sendServerPublicKey','data':{'serverPublicKey':serverPublicKey}}));
    clients.push(socket);
    socket.on('data',data=>{
        data = JSON.parse(data.toString());
        if(data['on']=="sendMessage"){
            let message = data['data']['message'];
            message = crypto.crack(serverPrivateKey,message);
            message = JSON.parse(message);
            broadcast(socket,message);
            console.log(`${message.fullname} : ${message.message}`);
            
        }
        else if(data['on'] == "sendMyInfo"){            
            clients[clients.length-1] = {socket:socket,publicKey : data['data']['publicKey']};
        }
        else{}
    })
})
server.on('error',err =>{
    console.log("ERROR",err);
})
server.listen({port : 3000 , host : '127.0.0.1'});

function broadcast(sender,message){    
    clients.forEach(i=>{
        if(i['socket'] !== sender) {
            i['socket'].write(JSON.stringify({
                'on' : 'getMessage',
                'data' : {
                    'message' : crypto.hash(i.publicKey,JSON.stringify(message))
                }
            }));
        }
    })
}