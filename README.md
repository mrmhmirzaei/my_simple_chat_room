<h2>Install and run</h2>
<span>Step 1 : </span><code>npm i node-rsa</code><br>
<span>Step 2 : </span><code>node server</code><br>
<span>Step 3 : </span><code>node client</code>
<br><br>
<h2>Preview</h2>
<img src="https://gitlab.com/mrmhmirzaei/my_simple_chat_room/raw/master/Screen%20Shot.png"/>